import React from "react";
import { useNavigate } from "react-router-dom";

export default function Login() {
  const [loginFormData, setLoginFormData] = React.useState({
    email: "",
    password: "",
  });

  function handleSubmit(e) {
    e.preventDefault();
    console.log(loginFormData);
  }

  function handleChange(e) {
    const { name, value } = e.target;
    setLoginFormData((prev) => ({
      ...prev,
      [name]: value,
    }));
  }
  return (
    <div className="flex flex-col items-center justify-center p-5 gap-5 md:w-1/2 md:mx-auto md:bg-orange-100 rounded-xl m-5">
      <h1 className="font-bold text-3xl">Sign in to your account</h1>
      <form onSubmit={handleSubmit} className="text-center flex flex-col gap-3">
        <input
          className="p-2 border-zinc-300 active:border-2 rounded-md"
          name="email"
          onChange={handleChange}
          type="email"
          placeholder="Email Address"
          value={loginFormData.email}
          autoComplete="username"
        />
        <input
          className="p-2 border-zinc-300 active:border-2 rounded-md"
          name="password"
          onChange={handleChange}
          type="password"
          placeholder="Password"
          value={loginFormData.password}
          autoComplete="current-password"
        />
        <button className="py-3 px-32 font-bold bg-orange-500 text-white rounded-lg">
          Log in
        </button>
      </form>
    </div>
  );
}
