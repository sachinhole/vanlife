import React, { useEffect, useState } from "react";
import { Link, useLoaderData } from "react-router-dom";
import { getHostVans } from "../../../api/api";

// utils
import { authRequired } from "../../../utils/utils";

// get data
export async function loader() {
  return getHostVans();
}

export default function Hostvans() {
  const hostVans = useLoaderData();

  const hostVansEls = hostVans?.map((item) => (
    <Link
      to={item.id}
      key={item.id}
      className="flex h-28 p-5 bg-white rounded-md my-5"
    >
      <img src={item.imageUrl} alt="" className="rounded-md" />
      <div className="p-4 flex flex-col justify-center">
        <h2 className="font-bold text-lg">{item.name}</h2>
        <p className="font-semibold text-zinc-600">${item.price}/day</p>
      </div>
    </Link>
  ));

  return (
    <section className="container mx-auto px-5">
      <h1 className="font-bold text-3xl">Your listed vans</h1>
      <div className="">{hostVansEls}</div>
    </section>
  );
}
