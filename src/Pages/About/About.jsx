import React from "react";
import { Link } from "react-router-dom";

export default function About() {
  return (
    <>
      <img src="image/about.png" alt="" className="w-[100%]" />
      <div className="sm:w-3/4 mx-auto flex flex-col items-center p-5 lg:w-[60%]">
        <h2 className="font-bold text-3xl py-5 ">
          Don’t squeeze in a sedan when you could relax in a van.
        </h2>
        <p>
          Our mission is to enliven your road trip with the perfect travel van
          rental. Our vans are recertified before each trip to ensure your
          travel plans can go off without a hitch. (Hitch costs extra 😉)
        </p>
        <p className="py-3">
          Our team is full of vanlife enthusiasts who know firsthand the magic
          of touring the world on 4 wheels.
        </p>
        <div className="bg-orange-300 w-[100%] p-8 rounded-md">
          <h3 className="font-bold text-2xl">Your destination is waiting.</h3>
          <h3 className="font-bold text-2xl mb-5">Your van is ready.</h3>
          <Link
            to="/vans"
            className="font-bold bg-zinc-900 text-white p-3 rounded-lg"
          >
            Explore our vans
          </Link>
        </div>
      </div>
    </>
  );
}
