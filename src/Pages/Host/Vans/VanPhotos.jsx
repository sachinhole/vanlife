import React from "react";
import { useOutletContext } from "react-router-dom";

export default function VanPhotos() {
  const { van } = useOutletContext();
  return (
    <div className="px-5 pb-5">
      <img src={van.imageUrl} alt="" className="w-20 rounded-md" />
    </div>
  );
}
