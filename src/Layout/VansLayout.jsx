import React from "react";
import { NavLink } from "react-router-dom";

export default function VansLayout({ detail, price, image }) {
  const navItems = [
    { link: ".", name: "Details" },
    { link: "pricing", name: "Pricing" },
    { link: "photos", name: "Photos" },
  ];
  return (
    <>
      <div className="flex gap-3 p-5 pt-0 text-sm text-zinc-700 active:text-zinc-900 active:border-zinc-800 ">
        {navItems.map((item) => (
          <NavLink
            end
            key={item.name}
            className={({ isActive }) =>
              isActive
                ? "text-zinc-800 border-b-2 border-zinc-400 font-bold"
                : "text-zinc-500 hover:text-zinc-700"
            }
            to={item.link}
          >
            {item.name}
          </NavLink>
        ))}
      </div>
    </>
  );
}
