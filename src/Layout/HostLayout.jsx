import React from "react";
import { Outlet, NavLink } from "react-router-dom";

export default function Host() {
  const subNav = [
    { link: ".", name: "Dashboard" },
    { link: "income", name: "Income" },
    { link: "vans", name: "Vans" },
    { link: "reviews", name: "Reviews" },
  ];

  return (
    <section className="lg:w-3/4 mx-auto">
      <div className="flex gap-5 p-4 text-sm font-bold text-zinc-500 active:text-zinc-900 active:border-zinc-800 ">
        {subNav.map((item) => (
          <NavLink
            end={item.link === "." && true}
            key={item.name}
            className={({ isActive }) =>
              isActive
                ? "text-zinc-800 border-b-2 border-zinc-700 "
                : "text-zinc-500 hover:text-zinc-600"
            }
            to={item.link}
          >
            {item.name}
          </NavLink>
        ))}
      </div>
      <Outlet />
    </section>
  );
}
