import React from "react";
import { Link, Outlet } from "react-router-dom";

export default function Dashboard() {
  return (
    <section>
      <div className="bg-orange-100 p-5">
        <h2 className="font-black text-3xl py-5">Welcome!</h2>
        <div className="flex justify-between">
          <p className="text-zinc-600">
            Income last{" "}
            <span className="font-bold text-zinc-600 border-b border-zinc-900">
              30 days
            </span>
          </p>
          <Link>
            <p className="font-bold text-zinc-700">Details</p>
          </Link>
        </div>
        <h2 className="text-5xl font-black py-4">$2,260</h2>
      </div>
      <div className="flex justify-between items-center bg-orange-200 px-5 py-6">
        <h3 className="text-2xl font-bold">Review score</h3>
        <Link>
          <p className="font-bold text-zinc-700">Details</p>
        </Link>
      </div>

      <Outlet />
    </section>
  );
}
