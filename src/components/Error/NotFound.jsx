import React from "react";
import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <section className="h-[81vh] flex items-center justify-center p-5">
      <div className="text-center p-5 mb-10">
        <h1 className="font-black text-3xl mb-10">
          Sorry, the page you are looking for was not found.
        </h1>
        <Link
          to="/"
          className="bg-zinc-900 text-white font-bold text-xl p-3 px-10 rounded-lg"
        >
          Return to home
        </Link>
      </div>
    </section>
  );
}
