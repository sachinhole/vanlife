import React from "react";

export default function Footer() {
  return (
    <div className="h-24 bg-zinc-800 flex justify-center items-center">
      <p className="text-slate-300 text-md font-semibold">
        &#169; 2023 #VANLIFE
      </p>
    </div>
  );
}
