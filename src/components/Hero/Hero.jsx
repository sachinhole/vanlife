import React from "react";
import { Link } from "react-router-dom";

export default function Hero() {
  return (
    <div className="h-[70vh] bg-[url('/image/hero.png')] bg-cover bg-center bg-no-repeat w-full">
      <div className="container h-[100%] p-5 mx-auto flex flex-col items-center justify-center gap-4">
        <h1 className="text-5xl font-black text-white leading-14">
          You got the travel plans, we got the travel vans.
        </h1>
        <p className="text-white">
          Add adventure to your life by joining the #vanlife movement. Rent the
          perfect van to make your perfect road trip.
        </p>
        <Link
          to="/vans"
          className="p-3 bg-orange-500 text-white font-bold w-[100%] lg:w-[30%] rounded-md my-5 text-center"
        >
          Find your van
        </Link>
      </div>
    </div>
  );
}
