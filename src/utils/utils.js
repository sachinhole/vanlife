import { redirect } from "react-router-dom"

async function authRequired() {
    const isLoggedIn = false;
    
    if (!isLoggedIn) {
        throw redirect("/login");
    }
    return null;
}

export { authRequired };