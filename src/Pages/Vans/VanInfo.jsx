import React, { useEffect, useState } from "react";
import { useParams, Link, useLocation, useLoaderData } from "react-router-dom";
import { getVans } from "../../api/api";

// get data
export function loader({ params }) {
  return getVans(params.id);
}

export default function VanInfo() {
  const van = useLoaderData();
  const loacation = useLocation();

  return (
    <div className="p-5 lg:w-3/4 mx-auto">
      <Link
        to={`..${loacation.state?.search}`}
        relative="path"
        className="font-bold text-gray-600 border-b border-gray-500 my-5"
      >
        {`Back to ${loacation.state?.type || "all"} vans`}
      </Link>
      <div className="lg:flex">
        <img src={van.imageUrl} alt="" className="my-5 lg:max-w-2xl mx-auto" />
        <div className="py-3 lg:p-6">
          <button className="px-3 p-1 rounded-sm bg-orange-400">
            {van.type}
          </button>
          <h2 className="font-bold text-2xl tracking-tight py-3">{van.name}</h2>
          <p className="font-bold text-lg">
            ${van.price}
            <span className="text-sm font-semibold text-gray-600">/day</span>
          </p>
          <p className="tracking-tight py-1 leading-4 text-gray-700 text-sm">
            {van.description}
          </p>
          <Link className="bg-orange-500 p-2 mt-3 flex items-center justify-center rounded-sm font-bold text-white md:w-52">
            Rent this van
          </Link>
        </div>
      </div>
    </div>
  );
}
