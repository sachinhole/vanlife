import {
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
} from "react-router-dom";

// mirage dummy server
import "./server";

// pages
import Home from "./Pages/Home/Home";
import About from "./Pages/About/About";
import Vans, { loader as vansLoader } from "./Pages/Vans/Vans";
import VanInfo, { loader as vansInfoLoader } from "./Pages/Vans/VanInfo";
import Dashboard from "./Pages/Host/Dashboard/Dashboard";
import Income from "./Pages/Host/Income/Income";
import Reviews from "./Pages/Host/Reviews/Reviews";
import Hostvans, { loader as hostVansLoader } from "./Pages/Host/Vans/HostVans";
import HostVansInfo, {
  loader as HostVansInfoLoader,
} from "./Pages/Host/Vans/HostVanInfo";
import VanDetails from "./Pages/Host/Vans/VanDetails";
import VanPricing from "./Pages/Host/Vans/VanPricing";
import VanPhotos from "./Pages/Host/Vans/VanPhotos";
import NotFound from "./components/Error/NotFound";
import Error from "./components/Error/Error";
import Login from "./components/Login/Login";

// layoutes
import Layout from "./Layout/Layout";
import HostLayout from "./Layout/HostLayout";

// utils
import { authRequired } from "./utils/utils";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Layout />}>
      <Route index element={<Home />} />
      <Route path="about" element={<About />} />
      <Route path="login" element={<Login />} />

      <Route
        path="vans"
        element={<Vans />}
        errorElement={<Error />}
        loader={vansLoader}
      />
      <Route path="vans/:id" element={<VanInfo />} loader={vansInfoLoader} />

      <Route
        path="host"
        element={<HostLayout />}
        loader={async () => await authRequired()}
      >
        <Route index element={<Dashboard />} />
        <Route path="income" element={<Income />} />

        <Route path="vans" element={<Hostvans />} loader={hostVansLoader} />

        <Route
          path="vans/:id"
          element={<HostVansInfo />}
          loader={HostVansInfoLoader}
        >
          <Route index element={<VanDetails />} />
          <Route path="pricing" element={<VanPricing />} />
          <Route path="photos" element={<VanPhotos />} />
        </Route>

        <Route path="reviews" element={<Reviews />} />
      </Route>
      <Route path="*" element={<NotFound />} />
    </Route>
  )
);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
