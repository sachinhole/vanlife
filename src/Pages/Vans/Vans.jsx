import React, { useEffect, useState } from "react";
import { Link, useSearchParams, useLoaderData } from "react-router-dom";

// api
import { getVans } from "../../api/api";

// components
import Card from "../../components/Card/Card";

// get data
export function loader() {
  return getVans();
}

export default function Vans() {
  const [searchParams, setSearchParams] = useSearchParams();
  const vans = useLoaderData();

  // get type from url
  const typeFilter = searchParams.get("type");

  // filter
  const displayVans = typeFilter
    ? vans.filter((van) => van.type.toLowerCase() === typeFilter)
    : vans;

  // create element
  const vanElements = displayVans.map((van) => (
    <Link
      key={van.id}
      to={van.id}
      state={{ search: `?${searchParams.toString()}`, type: typeFilter }}
    >
      <Card key={van.id} item={van} />
    </Link>
  ));

  // merge setparames with setSearchParams
  function handleFilterChange(key, value) {
    setSearchParams((prevParams) => {
      if (value === null) {
        prevParams.delete(key);
      } else {
        prevParams.set(key, value);
      }
      return prevParams;
    });
  }

  return (
    <div className="md:w-3/4 mx-auto min-h-screen">
      <h1 className="font-extrabold text-xl mx-5">Explore our van options</h1>
      <div className="px-5 pt-2 pb-5 font-semibold text-sm text-zinc-700 space-x-3">
        <button
          onClick={() => handleFilterChange("type", "simple")}
          className={`${
            typeFilter === "simple" && "bg-orange-600 text-white"
          } bg-orange-100 px-3 py-1 rounded-sm hover:bg-orange-500 hover:text-white`}
        >
          Simple
        </button>
        <button
          onClick={() => handleFilterChange("type", "rugged")}
          className={`${
            typeFilter === "rugged"
              ? "bg-green-900 text-white"
              : "bg-orange-100"
          } px-3 py-1 rounded-sm hover:bg-green-800 hover:text-white`}
        >
          Rugged
        </button>
        <button
          onClick={() => handleFilterChange("type", "luxury")}
          className={`${
            typeFilter === "luxury" ? "bg-zinc-900 text-white" : "bg-orange-100"
          } px-3 py-1 rounded-sm hover:bg-zinc-700  hover:text-white`}
        >
          Luxury
        </button>
        {typeFilter && (
          <button
            onClick={() => handleFilterChange("type", null)}
            className="font-normal border-b border-zinc-700"
          >
            Clear filters
          </button>
        )}
      </div>
      <div className="flex flex-wrap justify-evenly">{vanElements}</div>
    </div>
  );
}
