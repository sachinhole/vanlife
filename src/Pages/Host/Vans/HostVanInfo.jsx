import React, { useEffect, useState } from "react";
import { Link, useParams, Outlet, useLoaderData } from "react-router-dom";

// api
import { getHostVans } from "../../../api/api";

// layout
import VansLayout from "../../../Layout/VansLayout";

// utils
import { authRequired } from "../../../utils/utils";

// get data
export async function loader({ params }) {
  return getHostVans(params.id);
}

export default function HostVansInfo() {
  const van = useLoaderData();

  return (
    <section className="container mx-auto p-5">
      <Link
        to=".."
        relative="path"
        className="border-b-2 border-zinc-500 text-zinc-600 font-semibold"
      >
        Back to all vans
      </Link>
      <div className="bg-white rounded-md mt-5">
        <div className="p-5 flex">
          <img src={van.imageUrl} alt="" className="rounded-sm w-32" />
          <div className="p-4 flex flex-col">
            <button className="px-2 p-1 w-16 text-sm font-semibold rounded-sm bg-orange-300">
              {van.type}
            </button>
            <h2 className="font-bold text-lg mt-2 mb-1">{van.name}</h2>
            <p className="font-bold">
              ${van.price}
              <span className="font-medium text-sm text-zinc-800">/day</span>
            </p>
          </div>
        </div>
        <VansLayout />
        <Outlet context={{ van }} />
      </div>
    </section>
  );
}
