import React from "react";
import { useOutletContext } from "react-router-dom";

export default function VanDetails() {
  const { van } = useOutletContext();
  return (
    <div className="px-5 text-[12px] flex flex-col gap-2 pb-4">
      <h5 className="font-bold">
        Name:<span className="font-medium"> {van.name}</span>
      </h5>
      <h5 className="font-bold">
        Category:<span className="font-medium"> {van.type}</span>
      </h5>
      <h5 className="font-bold lg:w-1/2">
        Description:
        <span className="font-medium"> {van.description}</span>
      </h5>
      <h5 className="font-bold">
        Visibility:<span className="font-medium"> public</span>
      </h5>
    </div>
  );
}
