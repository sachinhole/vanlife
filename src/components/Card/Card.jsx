import React from "react";

export default function Card({ item }) {
  return (
    <div className="w-40 lg:w-52 my-3 mx-1">
      <img
        src={item.imageUrl}
        alt="image van"
        className="rounded-md object-fit"
      />
      <div className="flex justify-between relative items-center mt-2">
        <h2 className="font-bold tracking-tight text-[15px] lg:text-lg">
          {item.name}
        </h2>
        <p className="font-bold tracking-tight">${item.price}</p>
        <p className="absolute right-0 top-5 text-[0.7rem] text-gray-600">
          /day
        </p>
      </div>
      <button
        className={`${
          item.type === "rugged"
            ? "bg-green-900 text-white"
            : item.type === "simple"
            ? "bg-orange-600 text-white"
            : "bg-zinc-900 text-white"
        } p-1 px-2 text-[12px] font-bold rounded-sm capitalize`}
      >
        {item.type}
      </button>
    </div>
  );
}
