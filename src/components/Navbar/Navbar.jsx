import React from "react";
import { NavLink, Link } from "react-router-dom";

import userImg from "../../assets/images/user.png";
import brandLogo from "/image/logog.png";

export default function Navbar() {
  const navItems = [
    { link: "/host", name: "Host" },
    { link: "/about", name: "About" },
    { link: "/vans", name: "Vans" },
  ];

  return (
    <div className="flex items-center justify-between h-24 px-5 bg-orange-50">
      <div className="container mx-auto flex justify-between items-center">
        <div>
          <Link to="/">
            {/* <h1 className="font-extrabold text-2xl not-italic">#VANLIFE</h1> */}
            <img src={brandLogo} alt="#VANLIFE" className="w-36" />
          </Link>
        </div>
        <div className="flex gap-5 font-bold text-sm text-zinc-500">
          {navItems.map((item) => (
            <NavLink
              to={item.link}
              key={item.name}
              className={({ isActive, isPending }) =>
                isActive
                  ? "text-zinc-800 border-b-2 border-zinc-700 "
                  : isPending
                  ? "animate-bounce ease-in-out text-zinc-700"
                  : "text-zinc-500 hover:text-zinc-600"
              }
            >
              {item.name}
            </NavLink>
          ))}
          <div className="flex items-center">
            <Link to="login">
              <img src={userImg} alt="user" className="w-5" />
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
