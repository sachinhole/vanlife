import React from "react";
import { useOutletContext } from "react-router-dom";

export default function VanPricing() {
  const { van } = useOutletContext();
  return (
    <div className="px-5 pb-8 pb">
      <h3 className="font-semibold text-lg">
        ${van.price}.00<span className="text-sm text-zinc-700">/day</span>
      </h3>
    </div>
  );
}
